//
//  CartItems.swift
//  Demo2
//
//  Created by My Mac on 18/05/1943 Saka.
//

class CartItems {
    static let shared = CartItems();
     var _items: [String] = [];
    func addItem(_ id: String?) {
        if (nil != id) {
            _items.append(id!);
        }
    }
    
    func removeItem(_ id: String?) {
        if let identifer  = id,
           let index = _items.firstIndex(of: identifer) {
            _items.remove(at: index);
        }
    }
    func hasItem(_ id: String?) -> Bool {
        if let identifer = id {
            return _items.contains(identifer)
        }
        return false;
    }
}

