//
//  CardView.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import Foundation
import UIKit

@IBDesignable
class CardView: UIView {
    static let textDarkColor = "#444444"
    @IBInspectable var cornerRadius: CGFloat = 2
    @IBInspectable var shadowOffsetWidth: Int = 0
    @IBInspectable var shadowOffsetHeight: Int = 3
    @IBInspectable var shadowColor = (ColorConverter.hexStringToUIColor(hex: ColorCode.textDarkColor)).cgColor
    @IBInspectable var shadowOpacity: Float = 0.5
    
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        let shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius)
        layer.masksToBounds = false
        layer.shadowColor = shadowColor
        layer.shadowOffset = CGSize(width: shadowOffsetWidth, height: shadowOffsetHeight);
        layer.shadowOpacity = shadowOpacity
        layer.shadowPath = shadowPath.cgPath
    }
    
@IBDesignable
class CardImageView: UIImageView {
    @IBInspectable var cornerRadius: CGFloat = 2
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
        
    }
}


@IBDesignable
class CardTableView: UITableView {
    @IBInspectable var cornerRadius: CGFloat = 26
    override func layoutSubviews() {
        layer.cornerRadius = cornerRadius
    }
  }
}

