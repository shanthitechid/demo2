//
//  ProductListTVC.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import UIKit

class ProductListVC: UIViewController,UITableViewDelegate,UITableViewDataSource, ProductTVCDelegate {

    @IBOutlet weak var tableView: UITableView!
   
     var selectedCell = Bool()
     var selectedIndx = IndexPath()
     var selectedSection = -1
     var menuArray = [HomeDataTypeModel]()
     
     // MARK: - View Life Cycles
     override func viewDidLoad() {
         super.viewDidLoad()
         self.tableView.delegate = self
         self.tableView.dataSource = self
         self.tableView.estimatedRowHeight = tableView.rowHeight
         self.tableView.rowHeight = UITableView.automaticDimension
         self.menuArray.append(HomeDataTypeModel(menuType: BANNER))
         self.menuArray.append(HomeDataTypeModel(menuType: HOME_MENU))
         // Do any additional setup after loading the view.
     }
     
     // MARK: - TableView Delegate Methods
     func numberOfSections(in tableView: UITableView) -> Int {
         return menuArray.count
     }
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         switch menuArray[section].menuType {
         case BANNER:
             return 1
         case HOME_MENU:
             return 1
         default:
             break
         }
         return 0
     }
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         switch menuArray[indexPath.section].menuType {
         case BANNER:
             let cell = tableView.dequeueReusableCell(withIdentifier: "BannerTVC", for: indexPath) as! BannerTVC
             
             return cell
         case HOME_MENU:
             let cell = tableView.dequeueReusableCell(withIdentifier: "ProductListTVC", for: indexPath) as! ProductListTVC
             cell.isSelectedCell = selectedCell
             cell.isSelectedCellIndex = selectedIndx
             cell.reloadData()
             cell.delegate = self
             
             return cell
         default:
             break
         }
         return UITableViewCell()
     }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
         switch menuArray[indexPath.section].menuType {
         
         case BANNER:
             return 150
             
         case HOME_MENU:
             
             let screenWidth = UIScreen.main.bounds.size.width
             let gridHeight = screenWidth/3
             let reqHeight = (screenWidth) + (170 * 1.2)
             print("conHeight2",gridHeight)
             print ("conHeight3", reqHeight)
             return reqHeight + 100
         default:
             return 0
         }
     }
     // MARK: - Model Info
     func passInfo(item: MenuModel) {
         let storyboard = UIStoryboard(name: "Main", bundle: nil)
         let controller = storyboard.instantiateViewController(withIdentifier: "ProductDetailsVC") as! ProductDetailsVC
         controller.menuInfo =  item;
         controller.onClose = {
             self.tableView.reloadData()
         }
         controller.modalPresentationStyle = .fullScreen
         self.present(controller, animated: true, completion: nil)
     }
     
 }



