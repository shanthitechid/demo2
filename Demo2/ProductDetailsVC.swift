//
//  ProductDetailsTVC.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import UIKit

class ProductDetailsVC: UIViewController {
    @IBOutlet weak var popOverLay: UIView!
    
    @IBOutlet weak var btnSelect: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var imageView: UIImageView!
    var menuInfo: MenuModel? = nil;
    var onClose: (() -> Void)? = nil;
    var selectedIndex = IndexPath()
    override func viewDidLoad() {
        super.viewDidLoad()

        lblTitle.text = title
        imageView.image = menuInfo?.image
        btnSelect.isSelected = CartItems.shared.hasItem(menuInfo?.identifier)
        
        if btnSelect.isSelected{
            btnSelect.setTitle("Selected", for: .normal)

        } else{
            btnSelect.setTitle("Select", for: .normal)

        }
    }
    @IBAction func cancelBtnPressed(_ sender: UIButton) {
        popOverLay.isHidden = true
    }
    
    @IBAction func okBtnPressed(_ sender: UIButton) {
        popOverLay.isHidden = true
         CartItems.shared.removeItem(menuInfo?.identifier);
         onClose?()
         self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func selectBtntapped(_ sender: UIButton) {
        
            if (btnSelect.isSelected){
                popOverLay.isHidden = false
    //            CartItems.shared.removeItem(menuInfo?.identifier);

                
            } else{
                CartItems.shared.addItem(menuInfo?.identifier);
                onClose?()
                self.dismiss(animated: true, completion: nil)

                
            }
    }
    
    @IBAction func backBtnPressed(_ sender: Any) {
        onClose?()
        self.dismiss(animated: true, completion: nil)
    }
}
