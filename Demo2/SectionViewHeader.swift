//
//  SectionViewHeader.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import UIKit
protocol SectionHeaderDelegate: class {
    func  buttonTapped(sender: UIButton)
}


class SectionViewHeader: UICollectionViewCell {

    @IBOutlet weak var sectionLabel: UILabel!
    @IBOutlet weak var btn: UIButton!
    weak var delegate:SectionHeaderDelegate?
    @IBAction func btnTapped(_ sender: UIButton) {
        delegate?.buttonTapped(sender: btn)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
