//
//  BannerModel.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import Foundation
import UIKit

struct BannerModel {
    public var img: UIImage!
}
class TestBannerdata{
static func getBanner() -> [BannerModel]{
      var bannerArray = [BannerModel]()
    bannerArray.append(BannerModel(img: UIImage(named: "bannerTwo")))
    bannerArray.append(BannerModel( img: UIImage(named: "bannerThree")))
      return bannerArray
  }
}
