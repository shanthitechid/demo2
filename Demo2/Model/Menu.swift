//
//  Menu.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import Foundation
import UIKit

struct MenuModel{
    var title: String
    var image: UIImage
    var identifier: String
}

