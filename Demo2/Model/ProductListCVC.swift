//
//  ProductListCVC.swift
//  Demo2
//
//  Created by My Mac on 18/05/1943 Saka.
//

import UIKit

class ProductListCVC: UICollectionViewCell {

    @IBOutlet var imageView: UIImageView!
    
    @IBOutlet var titleLabel: UILabel!
   
    
    @IBOutlet weak var consHeight: NSLayoutConstraint!
    @IBOutlet var overlayView: UIView!
   
    @IBOutlet weak var consWidth: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func load(item: MenuModel) {
        let screenWidth = UIScreen.main.bounds.size.width
        let screenHeight = UIScreen.main.bounds.size.height
        print(screenWidth)
        print(screenHeight)
        consWidth.constant = (screenWidth/3) - 20
        consHeight.constant = (screenHeight/2)
        print(consWidth.constant)
        print(consHeight.constant)
        imageView.image = item.image
        titleLabel.text = item.title

        overlayView.isHidden = !CartItems.shared.hasItem(item.identifier)
 
        
        
    }
}
