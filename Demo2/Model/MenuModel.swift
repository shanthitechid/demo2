//
//  MenuModel.swift
//  Demo2
//
//  Created by My Mac on 18/05/1943 Saka.
//

import Foundation
import UIKit

class ProductMetadata{
    
    class func getCarsMenu() -> [MenuModel]{
        var menuArray = [MenuModel]()
        menuArray.append(MenuModel(title: "Mahindra xuv", image: #imageLiteral(resourceName: "mahindra_xuv"), identifier : "1"))
        menuArray.append(MenuModel(title: "Hyundai i10", image: #imageLiteral(resourceName: "hyundai_ i10"),identifier : "2"))
        menuArray.append(MenuModel(title: "Maruthi swift", image: #imageLiteral(resourceName: "maruthi_swift"),identifier : "3"))
       /* menuArray.append(MenuModel(title: "Ford ikon", image: #imageLiteral(resourceName: "hyundai_ i10"),identifier : "4"))
        menuArray.append(MenuModel(title: "Honda city", image: #imageLiteral(resourceName: "honda_city"), identifier : "5"))
        menuArray.append(MenuModel(title: "Tata Nano", image: #imageLiteral(resourceName: "tata_nano"), identifier : "6"))
        menuArray.append(MenuModel(title: "Skoda Fabia", image: #imageLiteral(resourceName: "maruthi_swift"),identifier : "7"))
        menuArray.append(MenuModel(title: "Nissan Datasun", image: #imageLiteral(resourceName: "mahindra_xuv"), identifier : "8"))
        menuArray.append(MenuModel(title: "Toyata Innova", image: #imageLiteral(resourceName: "toyata_innova"), identifier : "9"))*/
        return menuArray
        
    }
    
    class func getBikesMenu() -> [MenuModel]{
        var menuArray = [MenuModel]()
        menuArray.append(MenuModel(title: "TVS Access", image: #imageLiteral(resourceName: "TVS Access"), identifier : "4"))
        menuArray.append(MenuModel(title: "Hero Maestro", image: #imageLiteral(resourceName: "yamaha_facino"), identifier : "5"))
        menuArray.append(MenuModel(title: "Honda Activa", image: #imageLiteral(resourceName: "Honda_Activa"), identifier : "6"))
       /* menuArray.append(MenuModel(title:"Yamaha facino", image: #imageLiteral(resourceName: "yamaha_facino"), identifier : "13"))
        menuArray.append(MenuModel(title: "Bajaj Pulsar", image: #imageLiteral(resourceName: "bajaj_pulsar"),identifier : "14"))
        menuArray.append(MenuModel(title: "Royal Enfield", image: #imageLiteral(resourceName: "Royal Enfield"), identifier : "15"))*/
        return menuArray
        
    }
    class func getCyclesMenu() -> [MenuModel] {
        var menuArray = [MenuModel]()
        menuArray.append(MenuModel(title: "Hero", image: #imageLiteral(resourceName: "Hecules"), identifier : "7"))
        menuArray.append(MenuModel(title: "Lady Bird", image: #imageLiteral(resourceName: "ladybird"), identifier : "8"))
        menuArray.append(MenuModel(title: "Hercules", image: #imageLiteral(resourceName: "Hero cycles"), identifier : "9"))
        return menuArray
    }
    
}

