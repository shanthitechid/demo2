//
//  ProductListTVC.swift
//  Demo2
//
//  Created by My Mac on 03/08/21.
//

import UIKit
protocol ProductTVCDelegate: class {
    func passInfo(item: MenuModel)
}

class ProductListTVC: UITableViewCell, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,SectionHeaderDelegate {
   
    
    
    
    var sectionTitles = [String]()
    weak var delegate: ProductTVCDelegate?
    var foodVC: ProductListCVC? = nil
    var isSelectedCell = Bool()
    var isSelectedCellIndex = IndexPath()

    @IBOutlet weak var productlistCollectionView: UICollectionView!
    
    var carArray = ProductMetadata.getCarsMenu()
    var bikeArray = ProductMetadata.getBikesMenu()
    var cycleArray = ProductMetadata.getCyclesMenu()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        sectionTitles = ["Car","Bikes","Cycles"]

        self.productlistCollectionView.register(UINib.init(nibName: "ProductListCVC", bundle: nil), forCellWithReuseIdentifier: "ProductListCVC")
        self.productlistCollectionView?.register(UINib(nibName: "SectionViewHeader", bundle: nil), forSupplementaryViewOfKind:UICollectionView.elementKindSectionHeader, withReuseIdentifier: "SectionViewHeader")

        self.productlistCollectionView.dataSource = self
        self.productlistCollectionView.delegate = self
    }

    func reloadData() {
        self.productlistCollectionView.reloadData();
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        
        if (isSelectedCell){
            
        let cell : ProductListCVC = productlistCollectionView.cellForItem(at: isSelectedCellIndex)! as! ProductListCVC
                cell.overlayView.isHidden = false

        }
        
    }

    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            if section == 0{
                return  carArray.count
            } else if section == 1{
                return bikeArray.count
            } else if section == 2{
                return cycleArray.count
            }else {
                return 0
            }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = productlistCollectionView.dequeueReusableCell(withReuseIdentifier: "ProductListCVC", for: indexPath as IndexPath) as! ProductListCVC
        
        if indexPath.section == 0{
            
            cell.load(item: carArray[indexPath.row])
            

        } else if indexPath.section == 1 {
            cell.load(item: bikeArray[indexPath.row])
            
        }else {
            cell.load(item: cycleArray[indexPath.row])
            
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if(section==0) {
           // return CGSize.zero
            return CGSize(width:collectionView.frame.size.width, height:30)

        } else if (section==1) {
            return CGSize(width:collectionView.frame.size.width, height:30)
        } else if (section==2) {
            return CGSize(width:collectionView.frame.size.width, height:30)
        } else {
            return CGSize(width:collectionView.frame.size.width, height:100)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "SectionViewHeader", for: indexPath) as? SectionViewHeader{
            sectionHeader.sectionLabel.text = sectionTitles[indexPath.section]
            sectionHeader.delegate = self;
            sectionHeader.btn.tag = indexPath.section
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            
            let item = carArray[indexPath.row]
            delegate?.passInfo(item: item);
            
        } else if indexPath.section == 1{
            let item = bikeArray[indexPath.row]
            delegate?.passInfo(item: item)
           
        }else {
            let item = cycleArray[indexPath.row]
            delegate?.passInfo(item: item)
        }
        

    }
    func buttonTapped(sender: UIButton) {
    }
 
   
    
}
